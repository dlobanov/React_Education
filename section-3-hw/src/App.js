import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import UserInput from './Models/UserInput'
import UserOutput from './Models/UserOutput'

class App extends Component {

    state = {
        objects: [
            {userName: "Planty user name!!!"}
            ]
    }

    inputChangedHandler = (event) => {
        this.setState({
            objects: [
                { userName: event.target.value }
            ]
        })
    }

    render() {
        return (
            <div className="App">
                <div>Section 3 HW</div>
                <UserInput name={this.state.objects[0].userName} changedHandler={this.inputChangedHandler}/>
                <UserOutput val={this.state.objects[0].userName}/>
            </div>
        );
    }
}

export default App;
