import React, { Component } from 'react';

class UserInput extends Component {

    render(props) {
        return (
            <div className="UserInput">
                <input
                    type="text" value={this.props.name}
                    onChange={this.props.changedHandler}
                />
            </div>
        )
    }
}

export default UserInput;