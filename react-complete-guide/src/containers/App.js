import React, { PureComponent } from 'react';
import classes from './App.css'
import './App.css';
import Persons from '../components/Persons/Persons'
import Cockpit from '../components/Cockpit/Cockpit'
import Aux from '../hoc/Aux'
import withClass from '../hoc/withClass'

export const AuthContext = React.createContext(false);

class App extends PureComponent {
    constructor(props) {
        super(props);

        console.log('[App.js] Inside constructor', props);

        this.state = {
            persons: [
                { id: 'as2f', name: 'Max', age: 28 },
                { id: 'dasd', name: 'Manu', age: 298 },
                { id: 'dasj', name: 'Stephanie', age: 268 },
            ],
            otherState: 'some other value',
            showPersons: false,
            toggleClicked: 0,
            authenticated: false
        }
    }

    loginHandler = () => {
        this.setState({authenticated: true});
    }

    componentWillMount() {
        console.log('[App.js] Inside componentWillMount');
    }

    componentDidMount() {
        console.log('[App.js] Inside componentDidMount');
    }

    componentWillReceiveProps(nextProps){
        console.log('[App.js] Inside componentWillReceiveProps', nextProps);
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     console.log('[App.js] Inside shouldComponentUpdate', nextProps, nextState);
    //     return nextState.persons !== this.state.persons ||
    //         nextState.showPersons !== this.state.showPersons;
    // }

    componentWillUpdate(nextProps, nextState) {
        console.log('[App.js] Inside componentWillUpdate', nextProps, nextState);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        console.log('[App.js] Inside getDerivedStateFromProps', nextProps, prevState);

        return prevState;
    }

    getSnapshotBeforeUpdate() {
        console.log('[App.js] Inside getSnapshotBeforeUpdate');

    }

    componentDidUpdate() {
        console.log('[App.js] Inside componentDidUpdate');
    }

    nameChangedHandler = (event, id) => {

        const personIndex = this.state.persons.findIndex(p => {
            // return p.id === id;
            return p.userId === id;
        });

        const person = {
            ...this.state.persons[personIndex]
        };

        person.name = event.target.value;

        const persons = [...this.state.persons];
        persons[personIndex] = person;

        this.setState({ persons: persons })
    }

    deletePersonHandler = (personIndex) => {
        const persons = [...this.state.persons];
        persons.splice(personIndex, 1);
        this.setState({persons: persons});
    }

    togglePersonsHandler = () => {
        const doesShow = this.state.showPersons;
        this.setState( (prevState, props) => {
            return {
                showPersons: !doesShow,
                toggleClicked: prevState.toggleClicked + 1
            }
        });
    }

    render() {
        console.log('[App.js] Inside render');

        let persons = null;

        if (this.state.showPersons) {
            persons = (
                <div>
                    <Persons
                        persons={this.state.persons}
                        clicked={this.deletePersonHandler}
                        changed={this.nameChangedHandler}
                    />
                </div>
            );
        }

        return (
            <Aux>
                <button onClick={() => {
                    this.setState({showPersons: true})
                }}>
                    Show Persons
                </button>
                <Cockpit
                    appTitle={this.props.title}
                    showPersons={this.state.showPersons}
                    persons={this.state.persons}
                    login={this.loginHandler}
                    clicked={this.togglePersonsHandler}/>
                <AuthContext.Provider value={this.state.authenticated}>
                    {persons}
                </AuthContext.Provider>
            </Aux>
        );

        // return React.createElement('div', null, React.createElement('h1', {className: 'App'}, 'Test text'))
    }
}

export default withClass(App, classes.App);
