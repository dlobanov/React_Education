import React, {Component} from 'react';
import classes from './Person.css';
import Aux from '../../../hoc/Aux'
import withClass from '../../../hoc/withClass'
import PropTypes from 'prop-types';
import { AuthContext} from "../../../containers/App";

class Person extends Component{

    constructor(props) {
        super(props);
        console.log('[Person.js] Inside constructor', props);
        this.inputElement = React.createRef();
    }

    componentWillMount() {
        console.log('[Person.js] Inside componentWillMount');
    }

    componentDidMount() {
        console.log('[Person.js] Inside componentDidMount');
        if (this.props.position === 0) {
            this.inputElement.current.focus();
        }
    }

    componentWillReceiveProps(nextProps){
        console.log('[Person.js] Inside componentWillReceiveProps');
    }

    render() {
        console.log('[Person.js] Inside render');

        return (
            <Aux>
                <AuthContext.Consumer>
                    {auth => auth ? <p>I'm authenticated!</p> : null }
                </AuthContext.Consumer>
                <p key="1" onClick={this.props.click}>
                    I'm {this.props.name} and I am {this.props.age} years old!
                </p>
                <p key="2">{this.props.children}</p>
                <input
                    ref={this.inputElement}
                    key="3"
                    type="text"
                    onChange={this.props.changed}
                    value={this.props.name}/>
            </Aux>
        );
    }

    focus() {
        this.inputElement.current.focus();
    }
}

Person.propTypes = {
    click: PropTypes.func,
    name: PropTypes.string,
    age: PropTypes.number,
    changed: PropTypes.bool
};

export default withClass(Person, classes.Person);